from request_url import *
from dom_hashing import *

request = retrieve_page('https://mybell.bell.ca/Login', 3)

with open('../data/test.txt', 'r') as f:
	test = f.read()



hash1 = find_hash(request)

hash2 = find_hash_text(test)

hash_score = compare_hash(hash1, hash2, 0.05, 0.1, 0.3, 0.7,["log in", "login", "log-in", "sign in", "sign-in", "sign up", "sign-up"], "script_keywords": ["login.php"])

print(hash_score)
