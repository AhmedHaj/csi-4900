import shodan
import censys.certificates
import time 


# Find similar websites using Shodan (retrieve hash, then search using hash)
def ext_cert_shodan(watchlist, shodan_api_key):
	api = shodan.Shodan(shodan_api_key)

	for domain in watchlist:
		# Search Shodan for watchlist domain
		results = api.search(domain)

		# Retrieve the hash value of the domain
		hash_value = results['matches'][0]['hash']



		time.sleep(0.25)

		# Search Shodan for hash value 
		hash_results = api.search('hash:' + str(hash_value))


		# if more than one result found, add non-watchlist result to certlist
		if len(hash_results['matches'] > 1):
			for result in hash_results['matches']:
				print(result['data'])
				print("hash:"+ str(result['hash']))
				print('')

		else:
			print("more than one yo")



# Find similar websites using Censys (search for Certificates issues by non-trusted CAs, and a given timestamp)
def ext_cert_censys(watchlist, UID, SECRET, certlist, non_trusted_org, validity_start):

	certificates = censys.certificates.CensysCertificates(UID, SECRET)
	fields = ["parsed.subject_dn", "parsed.fingerprint_sha256", "parsed.fingerprint_sha1"]


	for domain in watchlist:
		for org in non_trusted_org:
			query = '({}) AND parsed.issuer.organization.raw: "{}" and parsed.validity.start: [{} TO *]'.format(domain, org, validity_start)
					
			for c in certificates.search(query, fields=fields):
				domain = c["parsed.subject_dn"]
				if(domain not in certlist):
					certlist.append(domain)


	return certlist
			
