import json
import sys
import os
#sys.path.append('./dnstwist/')
import dnstwist

# Generates potential phishing domains using dnstwist
def generate_domains(domains):



	# Candidate list, which will be stored as a dict
	candidatelist = {}

	for domain in domains:
		# Command that will be run with dnstwist 
		command = 'python modules/dnstwist.py -r {} -f "json"'.format(domain)

		candidatelist[domain] = []
		print(domain)
		
		fdomains = os.popen(command).read()
		fdomains = json.loads(fdomains)
		for twist in fdomains:
			if twist['fuzzer'] != 'Original*':
				candidatelist[domain].append(twist)



	return candidatelist


