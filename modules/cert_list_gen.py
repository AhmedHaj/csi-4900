# Cert List Generation Module

from crtsh import crtshAPI
import json
import time

def generate_certlist_crt(candidatelist):
	certlist = {}
	domain_certlist = []


	for domain in candidatelist:
		for twist in candidatelist[domain]:
			if twist['fuzzer'] != 'original*':
				phishing_sus = twist['domain-name']
				certlist[phishing_sus] = crtshAPI().search(phishing_sus)
				time.sleep(0.25)
				print(phishing_sus)
				domain_certlist.append(phishing_sus)
				

	return (certlist, domain_certlist)

