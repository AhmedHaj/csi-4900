import shodan
import censys.certificates
import time 



# Find similar websites using Censys (search for Certificates issues by non-trusted CAs, and a given timestamp)
def ext_certlist_censys(watchlist, UID, SECRET, certlist_json, certlist_list, non_trusted_org, validity_start):

	certificates = censys.certificates.CensysCertificates(UID, SECRET)
	fields = ["parsed.subject_dn", "parsed.issuer_dn", "parsed.validity", "parsed.fingerprint_sha256", "parsed.fingerprint_sha1"]

	# For each website in the watch list, search Censys for similar websites
	for website in watchlist:
		#if(domain[-1]=='a'):
			for org in non_trusted_org:
				query = '({}*) AND parsed.issuer.organization.raw:"{}" and parsed.validity.start: [{} TO *]'.format(website, org, validity_start)
				#query = '({}*) AND parsed.issuer.organization.raw:"{}"'.format(website, org)
				print(query)
				time.sleep(0.25)
						
				for c in certificates.search(query=query, fields=fields):
					domain = c["parsed.subject_dn"][3:]

					# for each found domain, insert it and its Certificate Transparency Log to the cerlist
					# even if the domain was already in the certlist, it gets updated to include Censys's more accurate results
					certlist_json[domain] = {}
					certlist_json[domain]['issuer_name'] = c["parsed.issuer_dn"]
					certlist_json[domain]['not_before'] = c["parsed.validity.start"]
					certlist_json[domain]['not_after'] = c["parsed.validity.end"]
					certlist_list.append(domain)


	return (certlist_json, certlist_list)
			
