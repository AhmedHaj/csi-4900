# Cert List Generation Module

from crtsh import crtshAPI
import censys.certificates
import json

def generate_certlist_crt(candidatelist):
	certlist = {}
	for domain, twist in candidatelist:
		phishing_sus = twist['domain-name']
		certlist[phishing_sus] = crtshAPI().search(phishing_sus)

		


#def generate_certlist_censys(candidatelist, UID, SECRET):

#	certificates = censys.certificates.CensysCertificates(UID, SECRET)
#	fields = ["parsed.subject_dn", "parsed.fingerprint_sha256", "parsed.fingerprint_sha1"]

#	query = '(bell.ca) AND parsed.issuer.organization.raw: "Let\'s Encrypt" and parsed.validity.start: [2020-01-01 TO *]'

#	for c in certificates.search(query, fields=fields):
#		print(c["parsed.subject_dn"])
