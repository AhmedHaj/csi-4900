# CSI-4900

A Framework for the Detection of Phishing Websites

Install
-------

* Step 1: Install using pip: `sudo pip install -r requirements.txt`
* Step 2: Install httprobe: `▶ go get -u github.com/tomnomnom/httprobe`


Installing the Evaluation Module
-------

* Install using pip: `sudo pip install -r requirements.txt` in the main directory.


Configuration
--------------

The following attributes in `data/config.json` can be modified:

*  `“generate_domains”`: steps 1-3 take a long time to run, and need to be run primarily when using a new Watch List, can be set to false if the tool is simply rechecking the same Watch List that it used last time.
*  `"watchlist_path"`: path to watch list.
*  `"candidatelist_path"`: path to candidate list.
*  `"certlist_path"`: path to cert list.
*  `"extcertlist_path"`: path to ext cert list.
*  `"extcertlist_names_path"`: path to ext cert list domain name list (used by httprobe).
*  `"probedlist_path"`: path to probed list.
*  `"censys_UID"`: Censys UID value.
*  `"censys_SECRET"`: Censys SECRET value.
*  `"non_trusted_org"`: list of Certificate Authorities to search for on Censys.
*  `"validity_start"`: minimum registration date of TLS certificate that is used when searching for similar websites on Censys (set to a date very near to current date to avoid getting more than 1000 results if using the Free Censys API).


Configuration of the Evaluation Module
--------------

The following attributes in `evaluation_module/data/config.json` can be modified:

*  `"http_timeout_seconds"`: Number of seconds before timeout when retrieving requests object 
*  `"similarity_threshold"`: The score threshold that a website needs to be over in order to be included in the Phishing List.
*  `"score_less_15"`: The amount the score should be incremented by if a match between hashes was found, and the length of the matching text is less than 15 characters.
*  `"score_15"`: The amount the score should be incremented by if a match between hashes was found, and the length of the matching text is equal to or more than 15 characters.
*  `"score_keyword"`: The amount the score should be incremented by if a match between hashes was found, and a word in the list of keywords was found in the matching text. 
*  `"score_script_keyword"`: The score given to a website that redirects to a different website while including a word in the list of script_keywords.
*  `"keywords"`: List of keywords used for “score_keyword”.
*  `"script_keywords"`: List of keywords used for “score_script_keyword”  .
*  `"probedlist_path"`: path to probed list.
*  `"watchlist_path"`: path to watch list.
*  `"phishingcsv_path"`: output path to csv file.
*  `"extcertlist_path"`: path to ext cert list.
*  `"ipinfo_path"`: path to “IP2LOCATION-LITE-DB11.IPV6.BIN" that is used to retrieve data from IP addresses.
*  `"use_kibana"`: Whether or not Kibana is used.
*  `"kibana_index"`: Name of Kibana index that data should be uploaded to.
*  `"first_id"`: the first id that is set and to be incremented for when uploading to kibana (use 0 when switching to a new index, otherwise let it be because it gets updated each time data gets uploaded to Kibana




Running the Framework
-------

Simply run the following command: `python Framework.py` in the main directory of the file.


Running the Evaluation Module
-------

Simply run the following command: `python evaluation.py` in the `evaluation_module` directory of the file.










