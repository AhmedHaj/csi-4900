# Ahmed Haj Abdel Khaleq 8223727

import os
import io
import sys
sys.path.append('./modules/')
sys.path.append('./data/')
sys.path.append('./evaluation_module')
cwd = os.getcwd()
import evaluation_module
import json
from phishing_domain_gen import *
from cert_list_gen import *
from ext_certlist_gen import *
from evaluation import *
from urllib.parse import urlparse



def main():

	# Retrieve paths to website lists from config file
	config_path = './data/config.json'
	with open(config_path, 'r') as f:
		config = json.load(f)

	print(cwd)
	if not os.path.exists(config['watchlist_path']):
		print("ERROR: Watchlist not found at:", config['watchlist_path'])
		sys.exit()

	# Phase 1: retrieve watchlist as list
	print("Reading watchlist....")
	with open(config['watchlist_path'], 'r', encoding = "UTF-8") as f:
		watchlist = []
		src_file = f.readlines()
		for website in src_file:
			domain = website.strip('\n')
			hostname = urlparse(domain).netloc
			if(hostname not in watchlist):
				watchlist.append(hostname)

	# from config file, choose whether to execute phases 2-3 or not since phases 2-3 are required for new domains only
	if(config['generate_domains']):

		# Phase 2: generate phishing domains
		print("Generating a list of potential ​Phishing domains (Candidate List)....")
		candidatelist = generate_domains(watchlist)
		
		with open(config['candidatelist_path'], 'w', encoding = "UTF-8") as f:
			json.dump(candidatelist, f)

		# Phase 3: check Certificate Transparency logs for the websites in the candidate list
		print("Generating a list of Certificate Transparency logs for websites in the candidate list....")
		certlist = generate_certlist_crt(candidatelist)

		# write the dict into a json, certlist[1] is only the domain names without the CTLs to make the next phase more efficient
		with open(config['certlist_path'], 'w', encoding = "UTF-8") as f:
			json.dump(certlist[0], f)

	else:
		# read the premade lists (if config['generate_domains'] is false the it is assumed it has already been used to create the lists)
		with open(config['candidatelist_path'], 'r', encoding = "UTF-8") as f:
			candidatelist = json.load(f)

		certlist = []
		with open(config['certlist_path'], 'r', encoding = "UTF-8") as f:
			certlist.append(json.load(f))


		certlist.append([])



		for domain in certlist[0]:
			certlist[1].append(domain)



	# Phase 4: Search for similar websites using Shodan and Censys:
	print("Extending the list of Certificate List using Shodan and Censys....")

	# extract the domain names from watchlist, and use those to search on censys:
	domain_names_only = []
	for domain in watchlist:
		data = domain.split('.')
		if(data[1] not in ''.join(domain_names_only)):
			if(data[1] == "bell"):
				domain_names_only.append('bell*mobility')
			else: 
				domain_names_only.append(data[1])

	print(domain_names_only)


	ext_certlist = ext_certlist_censys(domain_names_only, config['censys_UID'], config['censys_SECRET'], certlist[0], certlist[1], config['non_trusted_org'], config['validity_start'])
	#print(ext_certlist[0])

	# write the dict into a json, ext_certlist[1] is only the domain names without the CTLs to allow for easy probing
	with open(config['extcertlist_path'], 'w', encoding = "UTF-8") as f:
		json.dump(ext_certlist[0], f)

	with open(config['extcertlist_names_path'], 'w', encoding = "UTF-8") as f:
		for domain in ext_certlist[1]:
			f.write("%s\n" % domain)

	# Phase 5: Probe the domains in the extended certlist for working HTTP/HTTPS servers
	print("Probing the Extended Certificate List for working HTTP/HTTPS servers...")

	command = "cat {}| httprobe ".format(config['extcertlist_names_path'])
	probedlist = os.popen(command).read()


	with open(config['probedlist_path'], 'w', encoding = "UTF-8") as f:
		f.write(probedlist)


	# clean up probed list	
	with open(config['probedlist_path'], 'r', encoding = "UTF-8") as f:
		probedlist = []
		src_file = f.readlines()
		for website in src_file:
			domain = website.strip('\n')
			if('https' in domain):
				probedlist.append(domain)


	with open(config['probedlist_path'], 'w', encoding = "UTF-8") as f:
		for domain in probedlist:
			f.write("%s\n" % domain)

	evaluation_module()


	




if __name__ == "__main__":
	main()



