from bs4 import BeautifulSoup
import mmh3

# finds the hash values for the DOM tree of a given request object, returns a list of hashes
def find_hash(request):
	hashes = {}

	soup = BeautifulSoup(request.content, features = 'lxml')
	for tag in soup.findAll(True):
		# get content of each tag 
		content = tag.get_text().strip()

		# find the murmur hash of the content
		hashed = mmh3.hash(content)



		# add hashed value to list of hashes
		if(hashed != 0):
			hashes[hashed] = content


	return hashes

# same as find_hash, but this uses a string instead of a request object (used for testing against old bell phishing websites)
def find_hash_text(text):
	hashes = {}
	soup = BeautifulSoup(text, features = 'lxml')
	for tag in soup.findAll(True):
		# get content of each tag 
		content = tag.get_text().strip()



		# find the murmur hash of the content
		hashed = mmh3.hash(content)


		# add hashed value to list of hashes
		if(hashed!=0):
			hashes[hashed] = content

	return hashes

# Compares a hash list from a suspect website to a hash list of the watchlist website, outputting a score
def compare_hash(original, suspect, score_less_15, score_15, score_keyword, score_script_keyword, keywords, script_keywords):
	score = 0

	for hash_val in suspect:
		matching_text = suspect[hash_val].lower()

		# Special case, if it the suspect page runs a log in script then it gets flagged
		for word in script_keywords:
				if word in matching_text:
					score+=score_script_keyword
					break;
					
		if hash_val in original.keys():

			# Adding score based on the length of the matching text
			if(len(matching_text) < 15):
				score += score_less_15

			else:
				score += score_15

			# Special case, if any of the keywords list in the config file are in the matching text, score increases by 0.1
			for word in keywords:
				if word in matching_text:
					score+=score_keyword
					break;

			if(score >= 1):
				return 1.0

		

	return score