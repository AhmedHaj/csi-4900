import os 
import sys
import requests


# retrieves a response object for a given url
def retrieve_page(url, http_timeout):
    print('[*] Retrieving target homepage at %s' % url)
    try:
        original_response = requests.get(url, timeout=http_timeout)
    except requests.exceptions.Timeout:
        sys.stderr.write('[-] %s timed out after %d seconds.\n' % (url, http_timeout))
        return None
    except requests.exceptions.RequestException as e:
        sys.stderr.write('[-] Failed to retrieve %s\n' % url)
        return None
        

    if original_response.status_code != 200:
        print('[-] %s responded with an unexpected HTTP status code %d' % (url, original_response.status_code))
        return None
      

    if original_response.url != url:
        print('[*] "%s" redirected to "%s"' % (url, original_response.url))

    return original_response


