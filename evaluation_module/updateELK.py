import requests 
import json 
import os
import sys
from elasticsearch import Elasticsearch

def update_kibana(phishing_json, index_name, first_id):
    res = requests.get('http://localhost:9200')
    es = Elasticsearch([{'host': 'localhost', 'port': '9200'}])

    idNum = first_id
    for website in phishing_json:
        res = es.index(index=index_name, ignore=400, doc_type='_doc', id=idNum, body=website)
        print(res)
        print(idNum)
        idNum+=1
    return idNum



def create_index(index_name):
    mappings =  {"mappings" : {
                    "properties" : {
                    "IP" : {
                    "type" : "text",
                    "fields" : {
                    "keyword" : {
                    "type" : "keyword",
                    "ignore_above" : 256
                    }
                    }
                    },
                    "URL" : {
                    "type" : "text",
                    "fields" : {
                    "keyword" : {
                    "type" : "keyword",
                    "ignore_above" : 256
                    }
                    }
                    },
                    "country" : {
                    "type" : "text",
                    "fields" : {
                    "keyword" : {
                    "type" : "keyword",
                                "ignore_above" : 256
                                }
                            }
                        },
                        "expiration date" : {
                            "type" : "date"
                        },
                        "fragment" : {
                            "type" : "text",
                            "fields" : {
                            "keyword" : {
                            "type" : "keyword","ignore_above" : 256
                        }
                            }
                        },
                        "geo" : {
                          "type" : "geo_point"
                        },
                        "issuer name" : {
                            "type" : "text",
                            "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                                }
                            }
                        },
                        "path" : {
                            "type" : "text",
                            "fields" : {
                            "keyword" : {
                            "type" : "keyword",
                            "ignore_above" : 256
                            }
                          }
                        },
                        "query" : {
                            "type" : "text",
                            "fields" : {
                            "keyword" : {
                            "type" : "keyword",
                            "ignore_above" : 256
                                }
                            }
                        },
                        "registration date" : {
                            "type" : "date"
                        },
                        "score" : {
                           "type" : "float"
                        },
                        "timestamp" : {
                            "type" : "date"
                        }
                    }
                }
    }
    print(mappings)

    es = Elasticsearch([{'host': 'localhost', 'port': '9200'}])

    es.indices.create(index=index_name, body=mappings)



