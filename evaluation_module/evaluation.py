import os 
import sys
import json
cwd = os.getcwd()
from html_similarity import style_similarity, structural_similarity, similarity
from request_url import *
from dom_hashing import *
from urllib.parse import urlparse
import socket
import IP2Location
from datetime import datetime
from urllib.parse import urlparse
from updateELK import *
import csv




def evaluation_module():
	# set up files
	if(cwd[-1] == '0'):
		config_path = 'evaluation_module/' + 'data/config.json'

	else:
		config_path = './data/config.json'
	with open(config_path, 'r') as f:
		config = json.load(f)

	f.close()


	extcertlist_path = config['extcertlist_path']

	if(cwd[-1] == '0'):
		extcertlist_path = 'evaluation_module/' + extcertlist_path

	with open(extcertlist_path, 'r') as f:
		data = f.read()
		extcertlist = json.loads(data)

	f.close()

	ipdatabase = IP2Location.IP2Location()

	if(cwd[-1] == '0'):
		ipdatabase.open('evaluation_module/' + config['ipinfo_path'])
	else:
		ipdatabase.open(config['ipinfo_path'])




	# Step 1: read the probedlist (suspect list) and the watchlist
	probedlist_path = config['probedlist_path']
	if(cwd[-1] == '0'):
		probedlist_path = 'evaluation_module/' + probedlist_path

	with open(probedlist_path, 'r') as f:
		probedlist = []
		src_file = f.readlines()
		for website in src_file:
			domain = website.strip('\n')
			probedlist.append(domain)



	watchlist_path = config['watchlist_path']
	if(cwd[-1] == '0'):
		watchlist_path = 'evaluation_module/' + watchlist_path
	with open(watchlist_path, 'r') as f:
		watchlist = []
		src_file = f.readlines()
		for website in src_file:
			watchlist.append(website.strip('\n'))
			


	# Step 2: retrieve the requests object for websites in the watchlist, and hash the DOM trees of each website
	watchlist_requests = []
	for website in watchlist:
		request = retrieve_page(website, config['http_timeout_seconds'])
		if(request!= None):
			watchlist_requests.append(request)
		else:
			continue

	watchlist_hashes = [] # list containing dicts that contain the hash values of each DOM tree 
	for request in watchlist_requests:
		hash_dict = find_hash(request)
		watchlist_hashes.append(hash_dict)
			

	# Step 3: retrieve the requests object for websites in the probed list, and has their DOM trees
	probedlist_requests = []
	for website in probedlist:
		request = retrieve_page(website, config['http_timeout_seconds'])
		if(request != None):
			probedlist_requests.append(request)
		else:
			continue

			


	# Step 4: for each probed list website, find its DOM tree hash values, and compare to each watchlist website's DOM tree hash values.
	phishing_list = []
	phishing_json = []
	similarity_threshold = config['similarity_threshold']



	for request in probedlist_requests:

		# Compute its hash values
		hash_dict = find_hash(request)

		# counter used to retrieve corresponding request object for each hash_dict_watch value
		request_watchlist_num = 0
		
		for hash_dict_watch in watchlist_hashes:
			# check to see if the website has already been flagged as a phishing website
			if(request.url in phishing_list):
				break;

			hash_score = 0
			struct_similarity_score = 0

			# Compute hash score
			hash_score = compare_hash(hash_dict_watch, hash_dict, config['score_less_15'], config['score_15'], config['score_keyword'], config['score_script_keyword'], config['keywords'], config['script_keywords'])

			# Special case: if url redirects to another webpage, and has the domain name of a watchlist website in its domain name then it gets flagged
			if(len(request.history) > 0):
				watchlist_url = watchlist_requests[request_watchlist_num].url
				watchlist_domain_name = urlparse(watchlist_url).hostname
				watchlist_domain_name = watchlist_domain_name.split('.')[-2]


				suspect_domain_name = urlparse(request.history[0].url).hostname
				suspect_domain_name = suspect_domain_name.split('.')[-2]
				print(watchlist_domain_name)
				print(suspect_domain_name)
				if(watchlist_domain_name in suspect_domain_name):
					struct_similarity_score+=config['score_script_keyword']
					

			
			# compute structural similarity value
			#if(len(request.text)!= 0 and len(watchlist_requests[request_watchlist_num].text) != 0):
			#	struct_similarity_score = similarity(request.text, watchlist_requests[request_watchlist_num].text)


			request_watchlist_num += 1

			
			# Compare the hash score and the struct score, if either is greater than the similarity threshold, add it to the phishing list
			if(hash_score >= similarity_threshold or struct_similarity_score >= similarity_threshold):
				phishing_entry = {}
				score = max(hash_score, struct_similarity_score)
				phishing_entry['score'] = score

				
				# URL information
				full_url = request.url

				if(len(request.history)>0):
					full_url = request.history[0].url
					full_url_name = urlparse(request.history[0].url).netloc
					

				else:
					full_url_name = urlparse(full_url).netloc



				phishing_entry['URL'] = full_url
				phishing_entry['fragment'] = urlparse(full_url).fragment
				phishing_entry['path'] = urlparse(full_url).path
				phishing_entry['query'] = urlparse(full_url).query

				

				# Certificate Transparency Log information
				if(not isinstance(extcertlist[full_url_name], list)):
					
					if(extcertlist[full_url_name] != None):
						phishing_entry['issuer name'] = extcertlist[full_url_name]["issuer_name"]
						phishing_entry['registration date']= extcertlist[full_url_name]['not_before'][:10]
						phishing_entry['expiration date'] = extcertlist[full_url_name]['not_after'][:10]
					else:
						break;


				else:
					if(len(extcertlist[full_url_name][0]) > 0):
						
						phishing_entry['issuer name'] = extcertlist[full_url_name][0][0]["issuer_name"]
						phishing_entry['registration date']= extcertlist[full_url_name][0][0]['not_before'][:10]
						phishing_entry['expiration date'] = extcertlist[full_url_name][0][0]['not_after'][:10]
					else: 
						
						#phishing_entry['issuer name'] = "C=US, O=Let's Encrypt, CN=Let's Encrypt Authority X3"
						#phishing_entry['registration date']= "2020-02-16"
						#phishing_entry['expiration date'] = "2020-05-16"
						#phishing_entry['score'] = 0.1
						break;
						


				# IP information
				hostname = urlparse(full_url).netloc
				ip = socket.gethostbyname(hostname)

				phishing_entry['IP'] = ip
				ipinfo = ipdatabase.get_all(ip)
				phishing_entry['geo'] = {}
				phishing_entry['geo']['lat'] = ipinfo.latitude
				phishing_entry['geo']['lon'] = ipinfo.longitude
				phishing_entry['country'] = ipinfo.country_long
				
				phishing_entry['timestamp'] = datetime.now().strftime("%Y-%m-%d")


				phishing_list.append(request.url)

				phishing_json.append(phishing_entry)
				

	# sort the results based on score
	phishing_json = sorted(phishing_json, key=lambda k: k['score'], reverse=True)

	# Step 5: write the phishing list into a csv file, and update kibana
	with open(config['phishingcsv_path'], 'w') as outfile:
		f = csv.writer(outfile)
		f.writerow(['score', 'URL', 'fragment', 'path', 'query', 'issuer name', 'registration date','expiration date', 'IP', 'geo', 'country', 'timestamp'])
		for entry in phishing_json:
			f.writerow(entry.values())





	# OPTIONAL. 

	#create_index(config['kibana_index'])

	if(config['use_kibana']):
		print('updating kibana ' + config['kibana_index'])
		config['first_id'] = update_kibana(phishing_json, config['kibana_index'], config['first_id'])
	
		with open(config_path, 'w') as f:
			json.dump(config, f)

		f.close()

	


if __name__ == "__main__":
	evaluation_module()



